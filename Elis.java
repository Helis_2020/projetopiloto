package Teste;
import robocode.*;
import java.awt.Color;

/**
 * Elis - a robot by (your name here)
 */
public class Elis extends Robot
{public void run() {
		  setColors(Color.yellow,Color.red,Color.yellow);
		while(true) {
			double i = (double) (40+Math.random()*400);
			double j = (double) (40+Math.random()*400);
			double k = (double) (1+Math.random()*180);
			ahead(i);
			back(j);
			turnRight(k);
			turnGunRight(360);
			setAdjustGunForRobotTurn(true);
		}
	}
	



	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		// Replace the next line with any behavior you would like
		fire(2);
	}
	


	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		ahead(50);
		back(80);
	}
	
	public void onHitRobot (HitRobotEvent e) {
		back(30);
	}
	
	/**
	 * onHitWall: What to do when you hit a wall
	 */
/*	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		back(20);
	}	*/
		
// onHitWall: É executado quando o robô colide com a parede.
	public void onHitWall 	(HitWallEvent e) {
		double x = getX();
		double y = getY();
		double m = getBattleFieldWidth();
		double n = getBattleFieldHeight();
		if((x==m)||(x==0)) {
		    ahead(100);
		}
		if((y==n)||(y==0)) {
		    back(40);
		}
}

 public void onWin(WinEvent e) {
        int uhuu = 1;
        while (true) {
            turnRight(180 * uhuu);
            uhuu = uhuu * -1;
        }
    }





}
